$.extend( $.fn.dataTableExt.oSort, {
"hungarian-pre": function ( a ) {
        var special_letters = {
            "A": "Aa", "a": "aa", "Á": "Ab", "á": "ab",
            "C": "Ca", "c": "ca",
            "E": "Ea", "e": "ea", "É": "eb", "é": "eb",
            "I": "Ia", "i": "ia", "Í": "Ib", "í": "ib",
            "N": "Na", "n": "na",
            "O": "Oa", "o": "oa", "Ó": "Ob", "ó": "ob", "Ö": "Oc", "ö": "oc", "Ő": "Od", "ő": "od", 
            "U": "Ua", "u": "ua", "Ú": "Ub", "ú": "ub", "Ü": "Uc", "ü": "uc", "Ű": "Ud", "ű": "ud"
        };
        for (var val in special_letters)
            a = a.split(val).join(special_letters[val]).toLowerCase();
        return a;
    },

    "hungarian-asc": function ( a, b ) {
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },

    "hungarian-desc": function ( a, b ) {
        return ((a < b) ? 1 : ((a > b) ? -1 : 0));
    }
} );